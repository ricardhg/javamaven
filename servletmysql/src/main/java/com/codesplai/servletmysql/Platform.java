package com.codesplai.servletmysql;


public class Platform {

    public int id_platform;
    public String platform;

   
    //CONSTRUCTORES CON ID
    public Platform(int id_platform, String platform){
        this.id_platform = id_platform;
        this.platform = platform;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", this.platform, this.id_platform);
    }
  
}