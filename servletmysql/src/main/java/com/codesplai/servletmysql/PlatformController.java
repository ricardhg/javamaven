
package com.codesplai.servletmysql;

import java.util.ArrayList;
import java.util.List;

// import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class PlatformController {
  
    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "platforms";
    private static final String KEY = "id_platform";

    public static List<Platform> getAll(){
        
        List<Platform> listaPlatforms = new ArrayList<Platform>();  // (1)
    
        String sql = String.format("select %s,platform from %s", KEY, TABLE); // (2)
            
        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { // (3)
    
        ResultSet rs = stmt.executeQuery(sql);  // (4)
        while (rs.next()) {
            Platform u = new Platform(
                        (Integer) rs.getObject(1),
                        (String) rs.getObject(2)); // (5)
            listaPlatforms.add(u);
            }
    
        } catch (Exception e) { // (6)
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaPlatforms; // (7)
    
     }
    



}