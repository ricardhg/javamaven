package com.codesplai.servletmysql;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.List;



public class PlataformasServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Platform> listaPlatforms = PlatformController.getAll();
        
     
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Plataformas</title>");
        out.println("<link rel=\"stylesheet\" href=\"css/estilos.css\">");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Plataformas</h1>");
        out.println("<table class='mitabla'><thead><tr><th>Id</th><th>Nombre</th></tr></thead>");
        out.println("<tbody>");

        for (Platform plataforma : listaPlatforms){
            out.println("<tr><td>"+plataforma.id_platform+"</td><td>"+plataforma.platform+"</td></tr>");
        }

        out.println("</tbody></table>");

        out.println("</body>");
        out.println("</html>");
    }
}