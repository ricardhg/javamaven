package com.codesplai.servletmysql;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;


public class HolaServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

       // "{"msg": "get", "ciudad": "barcelona"}

        out.println("{\"msg\": \"GET\", \"ciudad\":\"Barcelona\"}");
    }

    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        out.println("{\"msg\": \"POST\"}");
    }
}