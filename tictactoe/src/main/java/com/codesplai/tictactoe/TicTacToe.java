package com.codesplai.tictactoe;

import java.util.Random;

public class TicTacToe {

    private static String[] clases={"clase0", "clase1", "clase2"};


    public static String juegaJava(String posiciones){
        String nuevasPosiciones="";

        char[] elements = posiciones.toCharArray();
        int zeros = 0;
        for(char c : elements ) {
            if (c=='0'){
                zeros++;
            }
        }

        if(zeros==0) return posiciones;
        Random rnd = new Random();
        int randomZero = rnd.nextInt(zeros)+1;
        int i=0;
        for(char c : elements ) {
            if (c=='0') i++;
            if (c=='0' && randomZero==i){
                nuevasPosiciones += "2";
            } else {
                nuevasPosiciones += c;
            }
            
        }
        
        return nuevasPosiciones;
    }

    public static String[] getClases(String posiciones){
        
        String[] retorn = new String[9];

        char[] elements = posiciones.toCharArray();
        int i = 0;
        for(char c : elements ) {
            int numeric= c - '0';
            retorn[i]=TicTacToe.clases[numeric];
            i++;
        }
        return retorn;
    }


}