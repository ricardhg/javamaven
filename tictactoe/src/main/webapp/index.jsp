<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.tictactoe.TicTacToe" %>

<%
    String posiciones = request.getParameter("posiciones");
    if (posiciones==null || posiciones.length()!=9){ 
        posiciones = "000000000";
        }
    else {
        posiciones=TicTacToe.juegaJava(posiciones);
    }

    String[] clases = TicTacToe.getClases(posiciones);
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Java demo</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <link rel="stylesheet" href="css/estilos.css">
</head>
<body>

<div class="fila">
    <div id="00" class="celda <%= clases[0] %>"></div>
    <div id="01" class="celda <%= clases[1] %>"></div>
    <div id="02" class="celda <%= clases[2] %>"></div>
</div>
<div class="fila">
    <div id="03" class="celda <%= clases[3] %>"></div>
    <div id="04" class="celda <%= clases[4] %>"></div>
    <div id="05" class="celda <%= clases[5] %>"></div>
</div>
<div class="fila">
    <div id="06" class="celda <%= clases[6] %>"></div>
    <div id="07" class="celda <%= clases[7] %>"></div>
    <div id="08" class="celda <%= clases[8] %>"></div>
</div>


<a href="index.jsp">Volver a jugar</a>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script>

    posiciones="<%= posiciones %>";
    posarray = posiciones.split("");

    $(document).on("click", ".celda.clase0", function(){
        numeric_id=$(this).attr("id")*1;
        posarray[numeric_id]=1;
        window.location.href="index.jsp?posiciones="+posarray.join("");
    });


</script>


</body>
</html>