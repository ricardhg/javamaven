
package com.codesplai.vgames;

public class Genre {
    public int id_genre;
    public String genre;
    
    public Genre(int id, String nombre){
        this.id_genre=id;
        this.genre=nombre;
    }

    public Genre( String nombre){
       
        this.genre=nombre;
    }
}
