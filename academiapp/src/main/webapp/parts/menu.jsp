<%@page contentType="text/html;charset=UTF-8" %>

<%
  //inicializamos las variables loginId y loginName, por el momento sin valor
  int loginId=0;
  String loginName=null;


  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }
 

%>

<nav class="navbar navbar-expand-lg  navbar-dark bg-primary">
    <a class="navbar-brand" href="#">AcademiApp</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/academiapp">Inicio</a>
        </li>
    
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Alumnos
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/academiapp/alumno/list.jsp">Listado</a>
            <a class="dropdown-item" href="/academiapp/alumno/js_list.jsp">Listado JS</a>
            <% if (loginId>0) { %>
              <a class="dropdown-item" href="/academiapp/alumno/create.jsp">Nuevo Alumno</a>
            <% } %>
          </div>
        </li>
      

            
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Cursos
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/academiapp/curso/list.jsp">Listado</a>
            <a class="dropdown-item" href="/academiapp/curso/create.jsp">Nuevo Curso</a>
          </div>
        </li>
      
            
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Profesores
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/academiapp/profesor/list.jsp">Listado</a>
            <a class="dropdown-item" href="/academiapp/profesor/create.jsp">Nuevo Profesor</a>
          </div>
        </li>
      
    

      </ul>
      
   <ul class="navbar-nav">
        <li class="nav-item">
            <% if (loginId<1) { %>
                <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal">
                Login
                </a>
            <% } else { %>
            <form action="#" method="POST" id="logoutForm">
                <input type="hidden" name="logoutpost" value="modal">
                <a class="nav-link" id="logoutFormLaunch" href="#">Logout <%= loginName %></a>
              </form>
            <% } %>
        </li>
    </ul>
    

    </div>
  </nav>
  
