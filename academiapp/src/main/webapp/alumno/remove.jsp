<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%

boolean todook=false;
int loginId=0;

  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            todook=true;
          }
      }
 

    String id = request.getParameter("id");
    if (id==null || todook==false) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/academiapp/index.jsp");
    } else {
        int id_numerico = Integer.parseInt(id);
        AlumnoController.removeId(id_numerico);
        response.sendRedirect("/academiapp/alumno/list.jsp");
    }

%>
