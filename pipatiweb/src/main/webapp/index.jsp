<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.pipatiweb.HtmlFactory" %>

<%
    HtmlFactory factory = new HtmlFactory();
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Java demo</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    
</head>
<body>

<%= factory.titulo("Piedra, Papel o Tijera?") %>

<ul>
    <li><a href="jugada.jsp?jugada=1">Piedra</a></li>
    <li><a href="jugada.jsp?jugada=2">Papel</a></li>
    <li><a href="jugada.jsp?jugada=3">Tijera</a></li>
</ul>

</body>
</html>