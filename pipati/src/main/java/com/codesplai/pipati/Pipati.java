
package com.codesplai.pipati;

import java.util.Random;
import java.util.Scanner;

public class Pipati {

    public static Scanner teclado = new Scanner(System.in);
    private static String[] jugadas = {"Piedra", "Papel", "Tijera"};

    /**
     * juego Piedra Papel Tijera
     */
    public static void play(){
       
        Random rnd = new Random();

        System.out.println();
        System.out.print("Piedra (1), Papel (2) o Tijera (3)? ");

        int jugada_usuario = teclado.nextInt(); 
        int jugada_ordenador = rnd.nextInt(3)+1;
        
        if (jugada_usuario==jugada_ordenador) {
            System.out.printf("Empate! yo también he sacado %s",
                jugadas[jugada_usuario-1]
            );
        } else if (jugada_usuario==1 && jugada_ordenador==2){
            System.out.printf("Pierdes con %s, yo he sacado %s",
                jugadas[jugada_usuario-1],
                jugadas[jugada_ordenador-1]
            );
        } else if (jugada_usuario==1 && jugada_ordenador==3){
            System.out.printf("Ganas con %s, yo he sacado %s",
                jugadas[jugada_usuario-1],
                jugadas[jugada_ordenador-1]
            );

        } else if (jugada_usuario==2 && jugada_ordenador==1){
            System.out.printf("Ganas con %s, yo he sacado %s",
                jugadas[jugada_usuario-1],
                jugadas[jugada_ordenador-1]
            );

        } else if (jugada_usuario==2 && jugada_ordenador==3){
            System.out.printf("Pierdes con %s, yo he sacado %s",
                jugadas[jugada_usuario-1],
                jugadas[jugada_ordenador-1]
            );

        } else if (jugada_usuario==3 && jugada_ordenador==1){
            System.out.printf("Pierdes con %s, yo he sacado %s",
                jugadas[jugada_usuario-1],
                jugadas[jugada_ordenador-1]
            );

        } else if (jugada_usuario==3 && jugada_ordenador==2){
            System.out.printf("Ganas con %s, yo he sacado %s",
                jugadas[jugada_usuario-1],
                jugadas[jugada_ordenador-1]
            );
        }

        System.out.println();
        System.out.println();

    }

}

